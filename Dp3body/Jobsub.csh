#########################################################################
# file Name: Jobsub.csh
# author: yangsl
# mail: yangsl@ihep.ac.cn
# created Time: 2020年03月18日 星期三 18时29分47秒
#########################################################################
#!/bin/tcsh -f
set pwdir=`pwd`
rm sub_* cut_* newbranch_* -rf
set InputFile1=${pwdir}/temp_cut_Dp3body.cxx
set InputFile2=${pwdir}/temp_newbranch_Dp3body.cxx
set InputFile3=${pwdir}/temp_sub.csh

@ file = 1
while ($file <= 1)
	set  OutputFile1 = cut_Dp3body_${file}.cxx
	set  OutputFile2 = newbranch_Dp3body_${file}.cxx
	set  OutputFile3 = sub_${file}.csh

	cp $InputFile1 $OutputFile1
	cp $InputFile2 $OutputFile2
	cp $InputFile3 $OutputFile3

	echo $OutputFile1
	echo $OutputFile2
	echo $OutputFile3

	sed -i 's|FILE|'"${file}"'|g' $OutputFile1
	sed -i 's|FILE|'"${file}"'|g' $OutputFile2
	sed -i 's|FILE|'"${file}"'|g' $OutputFile3

	hep_sub $OutputFile3
	@ file = `expr $file + 1`
end
