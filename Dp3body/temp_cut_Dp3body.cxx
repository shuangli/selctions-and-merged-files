/*************************************************************************
    > File Name: cut_KsKp.cxx
    > Author: yangshuangli
    > Mail: yangshuangli@ucas.ac.cn
    > Created Time: 2020年10月22日 星期四 10时23分02秒
 ************************************************************************/

#include<iostream>
#include "TFile.h"
#include "TChain.h"
#include "TCut.h"
#include "TLorentzVector.h"
#include "TString.h"
#include <fstream>
using namespace std;
void cut_Dp3body_FILE(){
    TStopwatch time;
    time.Start();
    TString Year = "2017";
    TString JobNum= "157";
    TString Mag = "MagUp";
    TString NameFile = "Dp3body";


	TCut cut = "Km_PIDK>8&&Pip1_PIDK<-5&&Pip2_PIDK<-5&&Dp_L0Global_TIS==1&&(Km_Hlt1TrackMVADecision_TOS==1||Pip1_Hlt1TrackMVADecision_TOS==1||Pip2_Hlt1TrackMVADecision_TOS==1)&&Dp_PT>2600&&Dp_PT<25000&&Pip2_PT>500&&Pip2_PT<11000&&Pip1_PT<16000&&(Dp_PX<(0.317*(Dp_PZ - 24000))&&(-Dp_PX)<(0.317*(Dp_PZ - 24000)))&&Dp_ETA>2.0&&Dp_ETA<4.5&&Pip2_ETA>2.0&&Pip2_ETA<4.5&&Km_ETA>2.0&&Km_ETA<4.5&&Dp_PT<20000&&Pip1_ETA>2.0&&Pip1_ETA<4.5";

	TChain* tc_out = new TChain("Dp2KmPipPip/DecayTree");
	tc_out->Add("/publicfs/lhcb/user/yangsl/gangadir/workspace/shuangli/LocalXML/"+JobNum+"/FILE/output/*.root");

	TFile* file_out = new TFile("Root/"+NameFile+Year+Mag+"_FILE.root", "recreate");
	TTree* tr_out = tc_out->CopyTree(cut);
	std::cout << "Total Entries1: " << tc_out->GetEntries() << std::endl;
	std::cout << "Total Entries2: " << tr_out->GetEntries() << std::endl;
	file_out->cd();
	tr_out->AutoSave();
	file_out->Close();
    time.Stop();
    cout<<"TotalTime= "<<time.CpuTime()<<"s"<<endl;
    exit(0);
}
