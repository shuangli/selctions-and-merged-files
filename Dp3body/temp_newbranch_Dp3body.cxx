/*************************************************************************
    > File Name: cut_KsKp.cxx
    > Author: yangshuangli
    > Mail: yangshuangli@ucas.ac.cn
    > Created Time: 2020年10月22日 星期四 10时23分02秒
 ************************************************************************/

#include<iostream>
#include "TFile.h"
#include "TChain.h"
#include "TCut.h"
#include "TLorentzVector.h"
#include "TString.h"
#include <fstream>
using namespace std;
void newbranch_Dp3body_FILE(){
    TStopwatch time;
    time.Start();

    TString Year = "2017";
    TString JobNum= "157";
    TString Mag = "MagUp";
    TString NameFile = "Dp3body";

    TChain *chain = new TChain("DecayTree");
    chain->Add("Root/"+NameFile+Year+Mag+"_FILE.root");

    Double_t Dp_ETA,Dp_Y,Dp_phi,Dp_OWNPV_CHI2,Dp_IPCHI2_OWNPV,Dp_P,Dp_PT,Dp_PE,Dp_PX,Dp_PY,Dp_PZ,Dp_M,Dp_TAU;
    Int_t Dp_ID,Pip2_ID,Km_ID,Pip1_ID;
    chain->SetBranchAddress ("Dp_ETA", &Dp_ETA);
    chain->SetBranchAddress ("Dp_Y", &Dp_Y);
    chain->SetBranchAddress ("Dp_phi", &Dp_phi);
    chain->SetBranchAddress ("Dp_OWNPV_CHI2", &Dp_OWNPV_CHI2);
    chain->SetBranchAddress ("Dp_IPCHI2_OWNPV", &Dp_IPCHI2_OWNPV);
    chain->SetBranchAddress ("Dp_P", &Dp_P);
    chain->SetBranchAddress ("Dp_PT", &Dp_PT);
    chain->SetBranchAddress ("Dp_PE", &Dp_PE);
    chain->SetBranchAddress ("Dp_PX", &Dp_PX);
    chain->SetBranchAddress ("Dp_PY", &Dp_PY);
    chain->SetBranchAddress ("Dp_PZ", &Dp_PZ);
    chain->SetBranchAddress ("Dp_M", &Dp_M);
    chain->SetBranchAddress ("Dp_ID", &Dp_ID);
    chain->SetBranchAddress ("Dp_TAU", &Dp_TAU);

    Double_t Pip2_ETA,Pip2_Y,Pip2_phi,Pip2_OWNPV_CHI2,Pip2_IPCHI2_OWNPV,Pip2_P,Pip2_PT,Pip2_PE,Pip2_PX,Pip2_PY,Pip2_PZ,Pip2_M,Pip2_TAU;
    chain->SetBranchAddress ("Pip2_ETA", &Pip2_ETA);
    chain->SetBranchAddress ("Pip2_Y", &Pip2_Y);
    chain->SetBranchAddress ("Pip2_phi", &Pip2_phi);
    chain->SetBranchAddress ("Pip2_OWNPV_CHI2", &Pip2_OWNPV_CHI2);
    chain->SetBranchAddress ("Pip2_IPCHI2_OWNPV", &Pip2_IPCHI2_OWNPV);
    chain->SetBranchAddress ("Pip2_P", &Pip2_P);
    chain->SetBranchAddress ("Pip2_PT", &Pip2_PT);
    chain->SetBranchAddress ("Pip2_PE", &Pip2_PE);
    chain->SetBranchAddress ("Pip2_PX", &Pip2_PX);
    chain->SetBranchAddress ("Pip2_PY", &Pip2_PY);
    chain->SetBranchAddress ("Pip2_PZ", &Pip2_PZ);
    chain->SetBranchAddress ("Pip2_M", &Pip2_M);
    chain->SetBranchAddress ("Pip2_ID", &Pip2_ID);

    Double_t Km_ETA,Km_Y,Km_phi,Km_OWNPV_CHI2,Km_IPCHI2_OWNPV,Km_P,Km_PT,Km_PE,Km_PX,Km_PY,Km_PZ,Km_M,Km_TAU;
    chain->SetBranchAddress ("Km_ETA", &Km_ETA);
    chain->SetBranchAddress ("Km_Y", &Km_Y);
    chain->SetBranchAddress ("Km_phi", &Km_phi);
    chain->SetBranchAddress ("Km_OWNPV_CHI2", &Km_OWNPV_CHI2);
    chain->SetBranchAddress ("Km_IPCHI2_OWNPV", &Km_IPCHI2_OWNPV);
    chain->SetBranchAddress ("Km_P", &Km_P);
    chain->SetBranchAddress ("Km_PT", &Km_PT);
    chain->SetBranchAddress ("Km_PE", &Km_PE);
    chain->SetBranchAddress ("Km_PX", &Km_PX);
    chain->SetBranchAddress ("Km_PY", &Km_PY);
    chain->SetBranchAddress ("Km_PZ", &Km_PZ);
    chain->SetBranchAddress ("Km_M", &Km_M);
    chain->SetBranchAddress ("Km_ID", &Km_ID);

    Double_t Pip1_ETA,Pip1_Y,Pip1_phi,Pip1_OWNPV_CHI2,Pip1_IPCHI2_OWNPV,Pip1_P,Pip1_PT,Pip1_PE,Pip1_PX,Pip1_PY,Pip1_PZ,Pip1_M,Pip1_TAU;
    chain->SetBranchAddress ("Pip1_ETA", &Pip1_ETA);
    chain->SetBranchAddress ("Pip1_Y", &Pip1_Y);
    chain->SetBranchAddress ("Pip1_phi", &Pip1_phi);
    chain->SetBranchAddress ("Pip1_OWNPV_CHI2", &Pip1_OWNPV_CHI2);
    chain->SetBranchAddress ("Pip1_IPCHI2_OWNPV", &Pip1_IPCHI2_OWNPV);
    chain->SetBranchAddress ("Pip1_P", &Pip1_P);
    chain->SetBranchAddress ("Pip1_PT", &Pip1_PT);
    chain->SetBranchAddress ("Pip1_PE", &Pip1_PE);
    chain->SetBranchAddress ("Pip1_PX", &Pip1_PX);
    chain->SetBranchAddress ("Pip1_PY", &Pip1_PY);
    chain->SetBranchAddress ("Pip1_PZ", &Pip1_PZ);
    chain->SetBranchAddress ("Pip1_M", &Pip1_M);
    chain->SetBranchAddress ("Pip1_ID", &Pip1_ID);


    TFile *newfile = new TFile("root/branch_"+NameFile+Year+Mag+"_FILE.root","recreate");
    TTree *newtree = new TTree("DecayTree","DecayTree");

    Double_t Dp_ETAnew,Dp_Ynew,Dp_phinew,Dp_OWNPV_CHI2new,Dp_IPCHI2_OWNPVnew,Dp_Pnew,Dp_PTnew,Dp_PEnew,Dp_PXnew,Dp_PYnew,Dp_PZnew,Dp_Mnew,Dp_TAUnew;
    Int_t Dp_IDnew,Pip2_IDnew,Km_IDnew,Pip1_IDnew;
    newtree->Branch ("Dp_ETA", &Dp_ETAnew);
    newtree->Branch ("Dp_Y", &Dp_Ynew);
    newtree->Branch ("Dp_phi", &Dp_phinew);
    newtree->Branch ("Dp_OWNPV_CHI2", &Dp_OWNPV_CHI2new);
    newtree->Branch ("Dp_IPCHI2_OWNPV", &Dp_IPCHI2_OWNPVnew);
    newtree->Branch ("Dp_P", &Dp_Pnew);
    newtree->Branch ("Dp_PT", &Dp_PTnew);
    newtree->Branch ("Dp_PE", &Dp_PEnew);
    newtree->Branch ("Dp_PX", &Dp_PXnew);
    newtree->Branch ("Dp_PY", &Dp_PYnew);
    newtree->Branch ("Dp_PZ", &Dp_PZnew);
    newtree->Branch ("Dp_M", &Dp_Mnew);
    newtree->Branch ("Dp_ID", &Dp_IDnew);
    newtree->Branch ("Dp_TAU", &Dp_TAUnew);

    Double_t Pip2_ETAnew,Pip2_Ynew,Pip2_phinew,Pip2_OWNPV_CHI2new,Pip2_IPCHI2_OWNPVnew,Pip2_Pnew,Pip2_PTnew,Pip2_PEnew,Pip2_PXnew,Pip2_PYnew,Pip2_PZnew,Pip2_Mnew,Pip2_TAUnew;
    newtree->Branch ("Pip2_ETA", &Pip2_ETAnew);
    newtree->Branch ("Pip2_Y", &Pip2_Ynew);
    newtree->Branch ("Pip2_phi", &Pip2_phinew);
    newtree->Branch ("Pip2_OWNPV_CHI2", &Pip2_OWNPV_CHI2new);
    newtree->Branch ("Pip2_IPCHI2_OWNPV", &Pip2_IPCHI2_OWNPVnew);
    newtree->Branch ("Pip2_P", &Pip2_Pnew);
    newtree->Branch ("Pip2_PT", &Pip2_PTnew);
    newtree->Branch ("Pip2_PE", &Pip2_PEnew);
    newtree->Branch ("Pip2_PX", &Pip2_PXnew);
    newtree->Branch ("Pip2_PY", &Pip2_PYnew);
    newtree->Branch ("Pip2_PZ", &Pip2_PZnew);
    newtree->Branch ("Pip2_M", &Pip2_Mnew);
    newtree->Branch ("Pip2_ID", &Pip2_IDnew);

    Double_t Km_ETAnew,Km_Ynew,Km_phinew,Km_OWNPV_CHI2new,Km_IPCHI2_OWNPVnew,Km_Pnew,Km_PTnew,Km_PEnew,Km_PXnew,Km_PYnew,Km_PZnew,Km_Mnew,Km_TAUnew;
    newtree->Branch ("Km_ETA", &Km_ETAnew);
    newtree->Branch ("Km_Y", &Km_Ynew);
    newtree->Branch ("Km_phi", &Km_phinew);
    newtree->Branch ("Km_OWNPV_CHI2", &Km_OWNPV_CHI2new);
    newtree->Branch ("Km_IPCHI2_OWNPV", &Km_IPCHI2_OWNPVnew);
    newtree->Branch ("Km_P", &Km_Pnew);
    newtree->Branch ("Km_PT", &Km_PTnew);
    newtree->Branch ("Km_PE", &Km_PEnew);
    newtree->Branch ("Km_PX", &Km_PXnew);
    newtree->Branch ("Km_PY", &Km_PYnew);
    newtree->Branch ("Km_PZ", &Km_PZnew);
    newtree->Branch ("Km_M", &Km_Mnew);
    newtree->Branch ("Km_ID", &Km_IDnew);

    Double_t Pip1_ETAnew,Pip1_Ynew,Pip1_phinew,Pip1_OWNPV_CHI2new,Pip1_IPCHI2_OWNPVnew,Pip1_Pnew,Pip1_PTnew,Pip1_PEnew,Pip1_PXnew,Pip1_PYnew,Pip1_PZnew,Pip1_Mnew,Pip1_TAUnew;
    newtree->Branch ("Pip1_ETA", &Pip1_ETAnew);
    newtree->Branch ("Pip1_Y", &Pip1_Ynew);
    newtree->Branch ("Pip1_phi", &Pip1_phinew);
    newtree->Branch ("Pip1_OWNPV_CHI2", &Pip1_OWNPV_CHI2new);
    newtree->Branch ("Pip1_IPCHI2_OWNPV", &Pip1_IPCHI2_OWNPVnew);
    newtree->Branch ("Pip1_P", &Pip1_Pnew);
    newtree->Branch ("Pip1_PT", &Pip1_PTnew);
    newtree->Branch ("Pip1_PE", &Pip1_PEnew);
    newtree->Branch ("Pip1_PX", &Pip1_PXnew);
    newtree->Branch ("Pip1_PY", &Pip1_PYnew);
    newtree->Branch ("Pip1_PZ", &Pip1_PZnew);
    newtree->Branch ("Pip1_M", &Pip1_Mnew);
    newtree->Branch ("Pip1_ID", &Pip1_IDnew);

    for (int i=0;i<chain->GetEntries();i=i+1){
        chain->GetEntry(i);
        Dp_ETAnew=Dp_ETA;
        Dp_Ynew=Dp_Y;
        Dp_phinew=Dp_phi;
        Dp_OWNPV_CHI2new=Dp_OWNPV_CHI2;
        Dp_IPCHI2_OWNPVnew=Dp_IPCHI2_OWNPV;
        Dp_Pnew=Dp_P;
        Dp_PTnew=Dp_PT;
        Dp_PEnew=Dp_PE;
        Dp_PXnew=Dp_PX;
        Dp_PYnew=Dp_PY;
        Dp_PZnew=Dp_PZ;
        Dp_Mnew=Dp_M;
        Dp_IDnew=Dp_ID;

        Pip2_ETAnew=Pip2_ETA;
        Pip2_Ynew=Pip2_Y;
        Pip2_phinew=Pip2_phi;
        Pip2_OWNPV_CHI2new=Pip2_OWNPV_CHI2;
        Pip2_IPCHI2_OWNPVnew=Pip2_IPCHI2_OWNPV;
        Pip2_Pnew=Pip2_P;
        Pip2_PTnew=Pip2_PT;
        Pip2_PEnew=Pip2_PE;
        Pip2_PXnew=Pip2_PX;
        Pip2_PYnew=Pip2_PY;
        Pip2_PZnew=Pip2_PZ;
        Pip2_Mnew=Pip2_M;
        Pip2_IDnew=Pip2_ID;

        Km_ETAnew=Km_ETA;
        Km_Ynew=Km_Y;
        Km_phinew=Km_phi;
        Km_OWNPV_CHI2new=Km_OWNPV_CHI2;
        Km_IPCHI2_OWNPVnew=Km_IPCHI2_OWNPV;
        Km_Pnew=Km_P;
        Km_PTnew=Km_PT;
        Km_PEnew=Km_PE;
        Km_PXnew=Km_PX;
        Km_PYnew=Km_PY;
        Km_PZnew=Km_PZ;
        Km_Mnew=Km_M;
        Km_IDnew=Km_ID;

        Pip1_ETAnew=Pip1_ETA;
        Pip1_Ynew=Pip1_Y;
        Pip1_phinew=Pip1_phi;
        Pip1_OWNPV_CHI2new=Pip1_OWNPV_CHI2;
        Pip1_IPCHI2_OWNPVnew=Pip1_IPCHI2_OWNPV;
        Pip1_Pnew=Pip1_P;
        Pip1_PTnew=Pip1_PT;
        Pip1_PEnew=Pip1_PE;
        Pip1_PXnew=Pip1_PX;
        Pip1_PYnew=Pip1_PY;
        Pip1_PZnew=Pip1_PZ;
        Pip1_Mnew=Pip1_M;
        Pip1_IDnew=Pip1_ID;

        newtree->Fill();
    }
    std::cout << "Total Entries1: " << chain->GetEntries() << std::endl;
    std::cout << "Total Entries2: " << newtree->GetEntries() << std::endl;

    newtree->Write("",TObject::kOverwrite);
    newfile->Close();

    time.Stop();
    cout<<"TotalTime= "<<time.CpuTime()<<"s"<<endl;
    exit(0);
}
